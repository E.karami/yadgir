# Yadgir

This project includes the implementation of a number of classic machine learning algorithms in  Jupyter notebook environment.

linear regression, 
softmax, 
multilayer perceptron, 
DBSCAN, 
agglomerative clustering, 
k-means, 
PCA,
anomaly detection
